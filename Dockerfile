FROM golang:1.15.4 AS builder
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN ls ..
RUN go mod download
COPY cmd .
RUN ls -alt
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /uli/
COPY --from=builder /app/app .
RUN chgrp -R 0 /uli && \
    chmod -R g=u /uli
RUN pwd && ls -al
CMD ["./app"]  
