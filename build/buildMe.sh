#!/bin/bash
set -x
VERSION=$1
IMAGENAME=learngo-datainjector

podman build -f ../Dockerfile -t $IMAGENAME:$VERSION 
imageid=$(podman images localhost/$IMAGENAME:$VERSION --format  "{{.Id}}" | tail -n 1)


podman login -u kubeadmin -p $(oc whoami -t) default-route-openshift-image-registry.apps-crc.testing
podman push $imageid docker://default-route-openshift-image-registry.apps-crc.testing/j7first/$IMAGENAME:$VERSION



