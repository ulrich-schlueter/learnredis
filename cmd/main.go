package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	radix "github.com/mediocregopher/radix/v3"
)

var redisKeyName = "SampleParameters"

// Mandel brot
type Mandel struct {
	Centerx    float64
	Centery    float64
	Zoom       float64
	Iterations int64
}

var examples = []Mandel{
	{-2.5, 1, 2, 100},
	{-0.7463, 0.1102, 0.005, 100},
	{-0.415, -0.683, 1.0 / 80.0, 1000},
	{0.28693186889504513, 0.014286693904085048, 6.349e-4, 1000},
	{-0.023130818687499735, -0.9989908810312501, 9.26420625E-5, 1000},
}

func inject(client radix.Client, m Mandel) (err error) {

	b, _ := json.Marshal(m)
	bstring := string(b)

	err = client.Do(radix.Cmd(nil, "LPUSH", redisKeyName, bstring))

	if err != nil {
		// handle error
		fmt.Println("Could Not do something " + err.Error())

	}
	return

}

// create a client for either single instance or cluster configurations
func createClient(redisPtr *string, auth *string) (client radix.Client) {

	customConnFunc := func(network, addr string) (radix.Conn, error) {
		return radix.Dial(network, addr,
			radix.DialTimeout(1*time.Minute),
			radix.DialAuthPass(*auth),
		)
	}

	//get a first connection and determine if we are in a cluster or not
	pool, err := radix.NewPool("tcp", *redisPtr, 10, radix.PoolConnFunc(customConnFunc))
	if err != nil {
		fmt.Println("Could Not Connect: " + err.Error())
		os.Exit(8)
	}

	var info string
	err = pool.Do(radix.Cmd(&info, "INFO", "CLUSTER"))
	splitstring := strings.Split(info, "\r\n")
	fmt.Println("Cluster Info String: " + info)
	fmt.Println("Cluster Enabled String: " + splitstring[1])

	if splitstring[1] == "cluster_enabled:0" {
		fmt.Println("Pool Client created.")
		return pool
	}

	fmt.Println("Is Cluster. ")
	poolFunc := func(network, addr string) (radix.Client, error) {
		return radix.NewPool(network, addr, 100, radix.PoolConnFunc(customConnFunc))
	}

	cluster, err := radix.NewCluster([]string{*redisPtr}, radix.ClusterPoolFunc(poolFunc))
	if err != nil {
		fmt.Println("Could Not Connect: " + err.Error())
		os.Exit(8)
	}

	fmt.Println("Cluster Client created.")
	return cluster
}

func main() {

	redisPtr := flag.String("redisconnection", "127.0.0.1:6379", "IP:Port of the redis api to use.")
	examplesPtr := flag.String("examplefile", "", "filename with sample data to use.")
	authPtr := flag.String("auth", "", "password if required.")
	flag.Parse()

	var exampleData []Mandel

	if *examplesPtr == "" {
		exampleData = examples
		fmt.Println("Examples:", "(local)")
	} else {
		jsonFile, err := os.Open(*examplesPtr)
		if err != nil {
			fmt.Println(err)
		}

		defer jsonFile.Close()

		byteValue, _ := ioutil.ReadAll(jsonFile)

		json.Unmarshal(byteValue, &exampleData)
		fmt.Println("Examples: from file \"" + *examplesPtr + "\"")
		fmt.Println(exampleData)
	}

	fmt.Println("Redis Connection:", *redisPtr)
	fmt.Println("Key:", redisKeyName)

	var client radix.Client

	client = createClient(redisPtr, authPtr)

	var len string
	err := client.Do(radix.Cmd(&len, "LLEN", redisKeyName))
	fmt.Println("#entries in key list: " + len)

	err = client.Do(radix.Cmd(nil, "DEL", redisKeyName))
	if err != nil {
		fmt.Println("Error Deleting key " + err.Error())
	}

	for _, m := range exampleData {
		inject(client, m)
		fmt.Println(m)

	}

	var bazEls []string
	err = client.Do(radix.Cmd(&bazEls, "LRANGE", redisKeyName, "0", "-1"))
	fmt.Println(bazEls)
}
